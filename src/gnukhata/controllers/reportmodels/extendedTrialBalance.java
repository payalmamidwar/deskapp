package gnukhata.controllers.reportmodels;

public class extendedTrialBalance {
private String srNo;
private String accountName;
private String groupName;
private String openingBalance;
private String totalDrTransactions;
private String totalCrTransactions;
private String drBalance;
private String crBalance;
//Class definition starts here, it is just a pojo with columns as properties.
//we have getters to actually return the respective value for the columns.
public extendedTrialBalance(String srNo, String accountName, String groupName,
		String openingBalance, String totalDrTransactions,
		String totalCrTransactions, String drBalance, String crBalance) {
	super();
	this.srNo = srNo;
	this.accountName = accountName;
	this.groupName = groupName;
	this.openingBalance = openingBalance;
	this.totalDrTransactions = totalDrTransactions;
	this.totalCrTransactions = totalCrTransactions;
	this.drBalance = drBalance;
	this.crBalance = crBalance;
}
public String getSrNo() {
	return srNo;
}
public String getAccountName() {
	return accountName;
}
public String getGroupName() {
	return groupName;
}
public String getOpeningBalance() {
	return openingBalance;
}
public String getTotalDrTransactions() {
	return totalDrTransactions;
}
public String getTotalCrTransactions() {
	return totalCrTransactions;
}
public String getDrBalance() {
	return drBalance;
}
public String getCrBalance() {
	return crBalance;
}





}
